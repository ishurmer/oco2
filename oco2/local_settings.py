DATABASES = {
    'default': {
         'ENGINE': 'django.contrib.gis.db.backends.postgis',
         'NAME': 'oco2',
         'USER': 'oco2',
         'PASSWORD': 'oco2',
         'HOST': '127.0.0.1'
     }
}

import logging
logging.basicConfig(level=logging.INFO)

BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

DEBUG=True