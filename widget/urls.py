from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^test-globe', views.TestWidgetView.as_view( ),
    	name='test-globe-widget')
]
