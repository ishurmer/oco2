from django.views.generic import TemplateView

class TestWidgetView(TemplateView):
	template_name = "widget/globe-test.html"