from django.conf import settings

OCO2_FTP_HOST = getattr(settings, 'OCO2_FTP_HOST',
					    'oco2.gesdisc.eosdis.nasa.gov')
OCO2_FTP_PATH = getattr(settings, 'OCO2_FTP_PATH',
						'/data/s4pa/OCO2_DATA/OCO2_L2_Standard.7/')
DOWNLOAD_PATH = getattr(settings, 'DOWNLOAD_PATH', 'oco2')
OCO2_FILE_EXTENSION = getattr(settings, 'OCO2_FILE_EXTENSION', '.h5')
NATURAL_EARTH_SRWKT = """GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",
	SPHEROID["WGS_1984",6378137.0,298.257223563]],PRIMEM["Greenwich",0.0],
	UNIT["Degree",0.017453292519943295]]"""