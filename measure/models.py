from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _

from . import settings

class L2MeasurementManagerQuerySet(models.QuerySet):
	def quality(self, choice=1):
		return self.filter(outcome_flag=choice)

class L2MeasurementManager(models.Manager):
	def get_by_natural_key(self, retrieval_time, lat_lng):
		return self.get(retrieval_time=retrieval_time, lat_lng=lat_lng)
	def get_queryset(self):
		return L2MeasurementManagerQuerySet(self.model, using=self._db)

class L2Measurement(models.Model):
	class Meta:
		ordering = ("-retrieval_time", )

	LAND_WATER_CHOICES = (
		(0, _("Land")), (1, _("Water")), (2, _("Unused")),
		(3, _("Mixed Land/Water"))
	)
	OUTCOME_CHOICES = (
		(1, _("Passed internal quality check")),
		(2, _("Failed internal quality check")),
		(3, _("Reached maximum allowed iterations")),
		(4, _("Reached maximum allowed divergences"))
	)
	objects = L2MeasurementManager( )
	def natural_key(self):
		return (self.retrieval_time, self.lat_lng)

	@classmethod
	def to_naturalearth_px(cls, lat_lng, image_width=10800, image_height=5400,
						   res=30, srwkt=settings.NATURAL_EARTH_SRWKT):
		pt = lat_lng.transform(srwkt, clone=True)
		x, y = pt.x * res, pt.y * res
		x = x + (image_width * 0.5)
		y = y + (image_height * 0.5)
		return (x, image_height - y)


	retrieval_time = models.DateTimeField(db_index=True, help_text=_(
		"Data acquisition time for the sounding based upon the three footprint"
		" times (source format is yyyy-mm-ddThh:mm:ss.mmmZ)"))
	sounding_id = models.CharField(max_length=40, unique=True,
		help_text=_("The sounding_id of the sounding containing the spectra "
			"used to perform the retrieval"))
	altitude = models.DecimalField(max_digits=14, decimal_places=7,
		help_text=_("Altitude of the sounding based on Earth topography, in "
					"metres."))
	altitude_uncert = models.DecimalField(max_digits=14, decimal_places=2,
		help_text=_("Uncertanity of the source Earth topography data, in "
					"metres."))
	aspect = models.DecimalField(max_digits=14, decimal_places=7,
		help_text=_("Orientation of the surface slope relative to the local "
					"North, in degrees."))
	azimuth = models.DecimalField(max_digits=14, decimal_places=7,
		help_text=_("Angle between local North and the projection of the LOS "
					"onto the Earth based on topography, in degrees."))
	zenith = models.DecimalField(max_digits=14, decimal_places=7,
		help_text=_("Angle between the normal to the Earth geoid and the "
			"projection of the LOS onto the Earth surface based on Earth "
			"topography, in degrees."))
	land_fraction = models.DecimalField(max_digits=10, decimal_places=5,
		help_text=_("Percentage of land surface type within the sounding."))
	land_water_indicator = models.PositiveSmallIntegerField(
		choices=LAND_WATER_CHOICES, help_text=_("Surface type at the sounding"
			"location."), db_index=True)
	
	relative_velocity = models.DecimalField(max_digits=14, decimal_places=7,
		help_text=_("Velocity of the spacecraft along the LOS (positive "
			"indicates spacecraft moving towards sounding location), in "
			"metres per second."))
	surface_roughness = models.DecimalField(max_digits=14, decimal_places=7,
		help_text=_("Standard deviation of the altitude within the sounding, "
			"in metres."))

	lat_lng = models.PointField(db_index=True, srid=4326, #WGS84
		help_text=_("The longitude and latitude of the sounding location, "
			"using the WGS84 SRS."))

	outcome_flag = models.PositiveSmallIntegerField(
		choices=OUTCOME_CHOICES, db_index=True, help_text=_(
			"Indicator of retrieval results."))

	xco2 = models.DecimalField(max_digits=14, decimal_places=7,
		help_text=_("Column-averaged CO2 dry air mole fraction."))
	xco2_uncert = models.DecimalField(max_digits=14, decimal_places=7,
		help_text=_("Uncertainty in column averaged CO2 dry air mole "
					"fraction."))
	xco2_uncert_noise = models.DecimalField(max_digits=14, decimal_places=7,
		help_text=_("Uncertainty in column averaged CO2 dry air mole "
					"fraction due to noise."))
	xco2_uncert_smooth = models.DecimalField(max_digits=14, decimal_places=7,
		help_text=_("Uncertainty in column averaged CO2 dry air mole "
					"fraction due to smoothing."))
	xco2_uncert_interf = models.DecimalField(max_digits=14,
		decimal_places=7, help_text=_("Uncertainty in column averaged CO2 dry "
			" air mole fraction due to smoothing."))

	wind_speed = models.DecimalField(max_digits=14,
		decimal_places=7, help_text=_("Retrieved Cox-Munk wind speed in "
			"metres per second."))
	wind_speed_uncert = models.DecimalField(max_digits=14,
		decimal_places=7, help_text=_("Uncertainity of etrieved Cox-Munk wind "
			"speed."))

	#These are 2D fields, maybe store them in future.
	#retrieved_dry_air_column_layer_thickness = models.DecimalField(
	#	max_digits=14, decimal_places=7, help_text=_("Retrieved vertical "
	#		"column of dry air per atmospheric layer."))
	#retrieved_wet_air_column_layer_thickness = models.DecimalField(
	#	max_digits=14, decimal_places=7, help_text=_("Retrieved vertical "
	#		"column of wet air per atmospheric layer."))

	def __str__(self):
		return "%s: %s=%s" % (self.retrieval_time, self.lat_lng, self.xco2)