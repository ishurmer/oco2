from celery import shared_task

import ftplib, logging, os

LOGGER = logging.getLogger(__name__)

@shared_task
def import_oco2_ftp_file(host, path, local_path, force=False,
						 chunk_bytes=1024*1024):
	basename = os.path.basename(path)
	local = os.path.join(local_path, basename)
	if not os.path.exists(local) or force:
		LOGGER.info("Downloading and processing file %s" % path)

	LOGGER.info("Connecting to FTP host %s" % host)

	f = None
	fl = None
	try:
		f = ftplib.FTP(host)
		f.login( )

		LOGGER.info("Changing to path %s" % os.path.dirname(path))
		f.cwd(os.path.dirname(path))

		LOGGER.info("Pulling file %s" % basename)
		fl = open(local, "wb")
		fh = f.retrbinary('RETR %s' % basename, fl.write)
		'''while True:
			block = fh.recv(chunk_bytes)
			if not block: break
			fl.write(block)
			fl.flush( )
			lfs = float(os.path.getsize(local))
			LOGGER.info("%s: Pulled %d MB so far." % (basename,
													  lfs / 1024.0 / 1024.0))'''

		lfs = float(os.path.getsize(local))
		LOGGER.info("Received file %s, approx. %.2f MB." % (basename,
			lfs / 1024.0 / 1024.0))
		fl.close( )

		from . import interop
		interop.import_from_l2std_file(local)

	except Exception as ex:
		LOGGER.error("Unhandled exception: %s" % ex)
		raise
	finally:
		if f:
			try:
				f.close( )
			except:
				pass

		if fl:
			try:
				fl.close( )
			except:
				pass