from django.db.models import Min, Max

from functools import reduce
import decimal, logging, math
from PIL import Image, ImageDraw

LOGGER = logging.getLogger(__name__)

''' Gradient functions adapted from http://jtauber.com/blog/2008/05/18/creating_gradients_programmatically_in_python/'''

def linear_gradient(start_value, stop_value, start_offset=0.0, stop_offset=1.0):
	return lambda offset: (start_value + ((offset - start_offset) / (
    									   stop_offset - start_offset) * (
    									   stop_value - start_value))) / 255.0

def gradient(DATA):
    def gradient_function(x, y):
        initial_offset = 0.0
        for offset, start, end in DATA:
            if y < offset:
                r = linear_gradient(start[0], end[0], initial_offset, offset)(y)
                g = linear_gradient(start[1], end[1], initial_offset, offset)(y)
                b = linear_gradient(start[2], end[2], initial_offset, offset)(y)
                return r, g, b
            initial_offset = offset
    return gradient_function

def plot_measured_points(queryset, image_width=10800, image_height=5400,
						 gradient_colours=[
						 	(0.05, (0x00, 0xEE, 0x00), (0x00, 0xFF, 0x00)),
						 	(0.95, (0xEE, 0x00, 0x00), (0x00, 0xEE, 0x00)),
						 	(1.0, (0xFF, 0x00, 0x00), (0xEE, 0x00, 0x00))
						 ], gradient_steps=100, value_attr='xco2',
						 opacity=220, resolution=20, blob_radius=10,
						 blob_steps=1, value_type="mean", depth=None):
	i = Image.new("RGBA", (image_width, image_height), (255, 255, 255, 0))
	draw = ImageDraw.Draw(i)
	rng = queryset.aggregate(min=Min(value_attr), max=Max(value_attr))
	m = rng['min']
	r = rng['max'] - rng['min']
	depth = decimal.Decimal(depth) if depth else r
	assert r <= depth, "The range must be less than or equal to the depth."
	grads = gradient(gradient_colours)
	col_cache = {}
	val_grads = []
	break_x = list(range(0, image_width, resolution))
	break_y = list(range(0, image_height, resolution))
	bxl = len(break_x)
	diam = int(blob_radius * 0.5)

	heatmap_data = []
	for y in break_y:
		yr = []
		for x in break_x:
			yr.append((x, y, []))
		heatmap_data.append(yr)
	t = queryset.count( )
	c = 0
	for obj in queryset:
		c += 1
		if c % 5000 == 0: LOGGER.info("Processing point %d of %d" % (c, t))
		value = getattr(obj, value_attr)
		if value == None: continue

		perc = round((value - m) / depth, 3)
		pos = obj.to_naturalearth_px(obj.lat_lng, image_width, image_height)
		x, y = pos[0] - (pos[0] % resolution), pos[1] - (pos[1] % resolution)
		row, col = int(y / resolution), int(x / resolution) - 1
		try:
			heatmap_data[row][col][-1].append((value, perc))
		except:
			raise

	if blob_steps:
		it = float(depth) / blob_steps
		opac = (opacity - 100) / blob_steps
	for row in heatmap_data:
		for col in row:
			if not len(col[-1]): continue
			if value_type == "min":
				val = min([c[0] for c in col[-1]])
			elif value_type == "max":
				val = max([c[0] for c in col[-1]])
			else:
				val = reduce(lambda x, y: x + y, [c[0] for c in col[-1]])
				val /= len(col[-1])
			val = (float(val) - float(m)) / float(depth)
			if val >= 1.0:
				LOGGER.warn("Out of bound value %s for %s" % (val, col))
				val = 0.9999999
			x, y = col[0], col[1]

			for l in range(blob_steps, 0, -1):
				val = val  - ((blob_steps - 1) * it)
				color = grads(0, val)

				color = tuple([int(i*255) for i in color])+(
					int(opacity - ((l-1) * opac)),)
				rad = blob_radius * l
				tl = int(x-rad), int(y-rad)
				br = int(x+rad), int(y+rad)
				draw.rectangle((tl, br), fill=color)

	return {
		'depth': depth, 'gradient_function': grads, 'image': i,
		'value_range': rng
	}

def composite_plot(pil_image, world_image_path, key=False, colour_depth=None,
				   colour_gradient=None, lng_bounds=None, lat_bounds=None,
				   world_lng_start=-179.98333, world_lat_start=89.9833333,
				   world_px_res=0.03333333333):
	image = Image.open(world_image_path)
	LOGGER.info("Opened NaturalEarth raster image %s with size %s" % (
		world_image_path, image.size))
	# TODO: We should really be extracting this from a TFW description,
	# but this is ok for now.
	world_lng_range = image.size[0] * world_px_res
	world_lat_range = image.size[1] * world_px_res
	world_lng_end = world_lng_start + world_lng_range
	world_lat_end = world_lat_start - world_lat_range
	LOGGER.info("With px res %s, lng range is %s->%s, lat range is %s->%s" % (
		world_px_res, world_lng_start, world_lng_end, world_lat_start,
		world_lat_end))

	'''rws = []
	r = 0
	c = 0
	for row in heatmap_data:
		c = 0
		for col in row:
			if col[-1] == 9999.0: continue
			rws.append("%s %s %s" % (r, c, col[-1]))
			c += 1
		r += 1

	f = open("/tmp/plot.dat", "w")
	f.write("\n".join(rws))
	f.close( )

	plot = """
set title "Heat Map generated from a file containing Z values only"
unset key
set tic scale 0

# Color runs from white to green
set palette rgbformula -7,2,-7
set cbrange [0:1]
set cblabel "Score"
unset cbtics

set xrange [-0.5:%(rowlen)s]
set yrange [-0.5:%(collen)s]

$map1 << EOD
%(map)s
EOD

set view map
splot '$map1' matrix with image""" 
	plot = plot % {'map': "\n".join(rws),
				   'rowlen': bxl, 'collen': len(break_y)}
	f = open("/tmp/plot.txt", "w")
	f.write(plot)
	f.close( )'''

