from django.conf import settings as django_settings
from django.contrib.gis.geos import Point
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
import h5py

import datetime, logging, os
from ftplib import FTP

LOGGER = logging.getLogger(__name__)

from . import models, settings, tasks

def import_files_from_ftp(host=settings.OCO2_FTP_HOST,
						  path=settings.OCO2_FTP_PATH, year=2015, 
						  extension=settings.OCO2_FILE_EXTENSION,
						  local_path=settings.DOWNLOAD_PATH, force=False):
	LOGGER.info("Connecting to FTP host %s" % host)

	f = FTP(host)
	f.login( )

	LOGGER.info("Changing to path %s" % path)
	f.cwd(path)
	LOGGER.info("Changing to sub-path %s" % year)
	pth = os.path.join(path, str(year))
	f.cwd(pth)
	dirs = f.nlst( )
	LOGGER.info("Listed %d separate paths" % len(dirs))

	local_path = os.path.join(django_settings.MEDIA_ROOT, local_path)

	elen = len(extension)
	for d in dirs:
		f.cwd(pth)
		f.cwd(d)
		files = f.nlst( )

		for fn in files:
			if fn[-elen:].lower( ) == extension.lower( ):
				LOGGER.info("Triggering import task for file %s" % fn)
				tasks.import_oco2_ftp_file.apply_async(args=[host,
					os.path.join(os.path.join(pth, d), fn),
					local_path, force])



def import_from_l2std_file(path,
	geo_fields=('altitude', 'altitude_uncert', 'aspect',
				'azimuth', 'zenith', 'land_fraction',
				'land_water_indicator', 'relative_velocity',
				'surface_roughness'),
	head_fields=[],
	res_fields=('xco2', 'xco2_uncert', 'xco2_uncert_noise',
				'xco2_uncert_interf', 'xco2_uncert_smooth',
				'wind_speed', 'wind_speed_uncert',
				'outcome_flag',
				'retrieved_dry_air_column_layer_thickness')):
	f = h5py.File(path, "r")

	geo = f['RetrievalGeometry']
	head = f['RetrievalHeader']
	res = f['RetrievalResults']

	max = geo['retrieval_latitude'].len( )

	LOGGER.info("Opened H5 file %s, with %s records." % (path, max))

	with transaction.atomic( ):
		for i in range(0, max):
			if i % 100 == 0:
				LOGGER.info("Processing record %s of %s" % (i, max))
			try:
				meas = models.L2Measurement.objects.get(
					sounding_id=head['sounding_id'][i])
				LOGGER.info("Retrieved existing measurement with sounding "
					" id %s" %(meas.sounding_id))
			except models.L2Measurement.DoesNotExist:
				meas = models.L2Measurement(sounding_id=head['sounding_id'][i])
				LOGGER.info("Creating new measurement with sounding id %s" % (
					meas.sounding_id))

			pt = Point(float(geo['retrieval_longitude'][i]),
					   float(geo['retrieval_latitude'][i]))
			meas.lat_lng = pt

			try:
				meas.retrieval_time = datetime.datetime.strptime(
					head['retrieval_time_string'][i].decode('utf-8'),
					"%Y-%m-%dT%H:%M:%S.%fZ",)
			except:
				raise

			for field in geo_fields:
				try:
					val = geo['retrieval_%s' % field][i]
				except KeyError:
					LOGGER.warn("Invalid RetrievalGeometry field with key %s"%(
						field)+". Rolling back.")
					raise
				setattr(meas, field, val.astype('float'))

			for field in head_fields:
				try:
					setattr(meas, field, head[field][i].astype('float'))
				except KeyError:
					LOGGER.warn("Invalid RetrievalHeader field with key %s"%(
						field)+". Rolling back.")
					raise

			for field in res_fields:
				try:
					setattr(meas, field, res[field][i].astype('float'))
				except KeyError:
					LOGGER.warn("Invalid RetrievalHeader field with key %s"%(
						field)+". Rolling back.")
					raise

			meas.save( )