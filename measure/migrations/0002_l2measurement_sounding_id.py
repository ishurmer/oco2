# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('measure', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='l2measurement',
            name='sounding_id',
            field=models.CharField(help_text='The sounding_id of the sounding containing the spectra used to perform the retrieval', max_length=40, unique=True, default=1),
            preserve_default=False,
        ),
    ]
