# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('measure', '0002_l2measurement_sounding_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='l2measurement',
            old_name='xco2_uncert_interference',
            new_name='xco2_uncert_interf',
        ),
    ]
