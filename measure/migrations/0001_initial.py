# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='L2Measurement',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('retrieval_time', models.DateTimeField(db_index=True, help_text='Data acquisition time for the sounding based upon the three footprint times (source format is yyyy-mm-ddThh:mm:ss.mmmZ)')),
                ('altitude', models.DecimalField(max_digits=8, decimal_places=7, help_text='Altitude of the sounding based on Earth topography, in metres.')),
                ('altitude_uncertainty', models.DecimalField(max_digits=8, decimal_places=2, help_text='Uncertanity of the source Earth topography data, in metres.')),
                ('aspect', models.DecimalField(max_digits=8, decimal_places=7, help_text='Orientation of the surface slope relative to the local North, in degrees.')),
                ('azimuth', models.DecimalField(max_digits=8, decimal_places=7, help_text='Angle between local North and the projection of the LOS onto the Earth based on topography, in degrees.')),
                ('zenith', models.DecimalField(max_digits=8, decimal_places=7, help_text='Angle between the normal to the Earth geoid and the projection of the LOS onto the Earth surface based on Earth topography, in degrees.')),
                ('land_fraction', models.DecimalField(max_digits=6, decimal_places=5, help_text='Percentage of land surface type within the sounding.')),
                ('land_water_indicator', models.PositiveSmallIntegerField(db_index=True, choices=[(0, 'Land'), (1, 'Water'), (2, 'Unused'), (3, 'Mixed Land/Water')], help_text='Surface type at the soundinglocation.')),
                ('relative_velocity', models.DecimalField(max_digits=9, decimal_places=7, help_text='Velocity of the spacecraft along the LOS (positive indicates spacecraft moving towards sounding location), in metres per second.')),
                ('surface_roughness', models.DecimalField(max_digits=8, decimal_places=7, help_text='Standard deviation of the altitude within the sounding, in metres.')),
                ('lat_lng', django.contrib.gis.db.models.fields.PointField(db_index=True, help_text='The longitude and latitude of the sounding location, using the WGS84 SRS.', srid=4326)),
                ('outcome_flag', models.PositiveSmallIntegerField(db_index=True, choices=[(1, 'Passed internal quality check'), (2, 'Failed internal quality check'), (3, 'Reached maximum allowed iterations'), (4, 'Reached maximum allowed divergences')], help_text='Indicator of retrieval results.')),
                ('xco2', models.DecimalField(max_digits=8, decimal_places=7, help_text='Column-averaged CO2 dry air mole fraction.')),
                ('xco2_uncert', models.DecimalField(max_digits=8, decimal_places=7, help_text='Uncertainty in column averaged CO2 dry air mole fraction.')),
                ('xco2_uncert_noise', models.DecimalField(max_digits=8, decimal_places=7, help_text='Uncertainty in column averaged CO2 dry air mole fraction due to noise.')),
                ('xco2_uncert_smoothing', models.DecimalField(max_digits=8, decimal_places=7, help_text='Uncertainty in column averaged CO2 dry air mole fraction due to smoothing.')),
                ('xco2_uncert_interference', models.DecimalField(max_digits=8, decimal_places=7, help_text='Uncertainty in column averaged CO2 dry  air mole fraction due to smoothing.')),
                ('wind_speed', models.DecimalField(max_digits=8, decimal_places=7, help_text='Retrieved Cox-Munk wind speed in metres per second.')),
                ('wind_speed_uncert', models.DecimalField(max_digits=8, decimal_places=7, help_text='Uncertainity of etrieved Cox-Munk wind speed.')),
                ('retrieved_dry_air_column_layer_thickness', models.DecimalField(max_digits=8, decimal_places=7, help_text='Retrieved vertical column of dry air per atmospheric layer.')),
            ],
        ),
    ]
