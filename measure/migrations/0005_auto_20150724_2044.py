# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('measure', '0004_auto_20150724_2019'),
    ]

    operations = [
        migrations.AlterField(
            model_name='l2measurement',
            name='altitude',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Altitude of the sounding based on Earth topography, in metres.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='altitude_uncert',
            field=models.DecimalField(max_digits=14, decimal_places=2, help_text='Uncertanity of the source Earth topography data, in metres.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='aspect',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Orientation of the surface slope relative to the local North, in degrees.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='azimuth',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Angle between local North and the projection of the LOS onto the Earth based on topography, in degrees.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='land_fraction',
            field=models.DecimalField(max_digits=10, decimal_places=5, help_text='Percentage of land surface type within the sounding.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='relative_velocity',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Velocity of the spacecraft along the LOS (positive indicates spacecraft moving towards sounding location), in metres per second.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='retrieved_dry_air_column_layer_thickness',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Retrieved vertical column of dry air per atmospheric layer.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='surface_roughness',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Standard deviation of the altitude within the sounding, in metres.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='wind_speed',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Retrieved Cox-Munk wind speed in metres per second.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='wind_speed_uncert',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Uncertainity of etrieved Cox-Munk wind speed.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='xco2',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Column-averaged CO2 dry air mole fraction.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='xco2_uncert',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Uncertainty in column averaged CO2 dry air mole fraction.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='xco2_uncert_interf',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Uncertainty in column averaged CO2 dry  air mole fraction due to smoothing.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='xco2_uncert_noise',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Uncertainty in column averaged CO2 dry air mole fraction due to noise.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='xco2_uncert_smoothing',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Uncertainty in column averaged CO2 dry air mole fraction due to smoothing.'),
        ),
        migrations.AlterField(
            model_name='l2measurement',
            name='zenith',
            field=models.DecimalField(max_digits=14, decimal_places=7, help_text='Angle between the normal to the Earth geoid and the projection of the LOS onto the Earth surface based on Earth topography, in degrees.'),
        ),
    ]
