# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('measure', '0003_auto_20150724_2013'),
    ]

    operations = [
        migrations.RenameField(
            model_name='l2measurement',
            old_name='altitude_uncertainty',
            new_name='altitude_uncert',
        ),
    ]
