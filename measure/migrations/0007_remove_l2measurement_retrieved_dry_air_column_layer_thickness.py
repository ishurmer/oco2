# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('measure', '0006_auto_20150724_2046'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='l2measurement',
            name='retrieved_dry_air_column_layer_thickness',
        ),
    ]
