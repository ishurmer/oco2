# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('measure', '0005_auto_20150724_2044'),
    ]

    operations = [
        migrations.RenameField(
            model_name='l2measurement',
            old_name='xco2_uncert_smoothing',
            new_name='xco2_uncert_smooth',
        ),
    ]
