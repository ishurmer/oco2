from django.contrib import admin

from . import models

class L2MeasurementAdmin(admin.ModelAdmin):
	list_display = (
		'retrieval_time', 'sounding_id', 'lat_lng', 'outcome_flag', 'xco2'
	)
	list_filter = ('outcome_flag', 'retrieval_time')
	search_fields = ('sounding_id', )

admin.site.register(models.L2Measurement, L2MeasurementAdmin)